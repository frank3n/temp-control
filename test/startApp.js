const app = require('../server/src/app');
const {containersSpecification} = require('../server/config');
const temperatureSensorsApi = require('../mocks/temperatureSensorsApi');

let appInstance;

module.exports = {
  before: done => {
    appInstance = app({temperatureSensorsApi, containersSpecification}).listen(3001, () => {
      console.log(`Temperature control app is running on port ${3001} and waiting for e2e tests to be run...`);
      done();
    });
  },
  after: () => {
    appInstance.close();
  }
};
