describe('Temperature control app', () => {
  describe('when loaded', () => {
    beforeEach((client, done) => {
      client
        .url('http://localhost:3001/index.html')
        .waitForElementPresent('[role="app-container"]', 3000);
      done();
    });

    afterEach((client, done) => {
      client.end(() => done());
    });

    const expectTemperaturesToEqual = (client, expectedTemperatures) =>
      expectedTemperatures.forEach((expectedTemperature, index) =>
        client.expect.element(`#app [role="beer-container"]:nth-child(${index+1}) [role="current-temp"]`)
        .text.to.equal(`Current temperature: ${expectedTemperature}°C`));

    it('should display beer container temperatures', client => {
      expectTemperaturesToEqual(client, [0, 2, 2, 4, 4, 6]);
    });

    describe('after a few seconds', () => {
      it('should update beer container temperatures', client => {
        expectTemperaturesToEqual(client, [2, 2, 4, 4, 6, 6]);
        client.pause(5000);
        expectTemperaturesToEqual(client, [0, 2, 2, 4, 4, 6]);
      });
    });
  });
});
