module.exports = {
  containersSpecification: [{
    minTemperature: 4,
    maxTemperature: 6,
    id: 1
  }, {
    minTemperature: 5,
    maxTemperature: 6,
    id: 2
  }, {
    minTemperature: 4,
    maxTemperature: 7,
    id: 3
  }, {
    minTemperature: 6,
    maxTemperature: 8,
    id: 4
  }, {
    minTemperature: 3,
    maxTemperature: 5,
    id: 5
  }, {
    minTemperature: 4,
    maxTemperature: 6,
    id: 6
  }]
};
