const app = require('./src/app');
const PORT = process.env.PORT || 3000;
const {containersSpecification} = require('./config');
const temperatureSensorsApi = require('../mocks/temperatureSensorsApi');

app({temperatureSensorsApi, containersSpecification}).listen(PORT, () => {
  console.log(`Temperature control app is running on port ${PORT}`);
});
