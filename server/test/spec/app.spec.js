const {expect, request} = require('chai');
const {Observable} = require('rxjs');
const {containersSpecification} = require('../../config');
const app = require('../../src/app');

describe('Temperature status app', () => {
  let temperatureStatusApp;

  const bootstrapApp = () => app({
    temperatureSensorsApi: {
      getTemperatures: () => Observable.of([2, 3, 5, 7, 9, 0])
    },
    containersSpecification
  });

  describe('when /temp-status is called', () => {
    beforeEach(() => {
      temperatureStatusApp = request(bootstrapApp());
    });

    it('should return current beer containers state', done => {
      temperatureStatusApp
        .get('/temp-status')
        .end((error, response) => {
          expect(response.status).to.equal(200);
          expect(response.body).to.deep.equal([{
            minTemperature: 4,
            maxTemperature: 6,
            id: 1,
            currentTemperature: 2
          }, {
            minTemperature: 5,
            maxTemperature: 6,
            id: 2,
            currentTemperature: 3
          }, {
            minTemperature: 4,
            maxTemperature: 7,
            id: 3,
            currentTemperature: 5
          }, {
            minTemperature: 6,
            maxTemperature: 8,
            id: 4,
            currentTemperature: 7
          }, {
            minTemperature: 3,
            maxTemperature: 5,
            id: 5,
            currentTemperature: 9
          }, {
            minTemperature: 4,
            maxTemperature: 6,
            id: 6,
            currentTemperature: 0
          }]);
          done();
        });
    });
  });
});
