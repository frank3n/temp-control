const express = require('express');
const path = require('path');
const extend = require('lodash/fp/extend');

const app = ({temperatureSensorsApi, containersSpecification}) => {
  const app = express();

  app.use(express.static(path.join(__dirname, '../../public')));

  const mergeContainerSpecsAndCurrentTemperatures = temperatures =>
    containersSpecification.map((spec, containerIndex) => extend(spec, {currentTemperature: temperatures[containerIndex]}));

  app.get('/temp-status', (request, response) => {
    temperatureSensorsApi.getTemperatures().take(1).map(mergeContainerSpecsAndCurrentTemperatures)
      .subscribe(containersInfo => response.send(containersInfo));
  });

  return app;
};

module.exports = app;
