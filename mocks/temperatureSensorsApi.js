const {Observable} = require('rxjs');
const range = require('lodash/fp/range');

module.exports = {
  sign: 1,
  getTemperatures: function() {
    this.sign = this.sign * -1;
    return Observable.of(range(1, 7).map(value => value + (value % 2) * this.sign));
  }
};
