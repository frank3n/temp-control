import React from 'react';
import {each} from 'lodash';
import forEach from 'lodash/fp/forEach';
import invoke from 'lodash/fp/invoke';
import keys from 'lodash/fp/keys';
import omitBy from 'lodash/fp/omitBy';
import size from 'lodash/fp/size';
import hasIn from 'lodash/fp/hasIn';

const dispose = forEach(invoke('unsubscribe'));
const isObservable = hasIn('subscribe');

const fliterOutObservables = props => {
  const regularProps = omitBy(isObservable, props);
  return size(regularProps) ? regularProps : null;
};

const makeReactive = ReactComponent => {
  class ReactiveComponent extends React.Component {
    constructor(props) {
      super(props);
      this.state = fliterOutObservables(props);
      this.subscriptions = [];
    }

    componentWillMount() {
      each(this.props, (prop, propName) => {
        if (isObservable(prop)) {
          this.subscriptions.push(prop.subscribe(propValue => {
            const newState = {};

            newState[propName] = propValue;
            this.setState(newState);
          }));
        }
      });
    }

    componentWillUnmount() {
      dispose(this.subscriptions);
    }

    render() {
      return (
        <ReactComponent {...this.state} {...fliterOutObservables(this.props)} />
      );
    }
  }

  return ReactiveComponent;
};

export default keys(React.DOM).reduce((reactiveTags, tag) => {
  reactiveTags[tag] = makeReactive(tag);
  return reactiveTags;
}, {});

