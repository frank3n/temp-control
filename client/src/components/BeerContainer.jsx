import React from 'react';
import cond from 'lodash/fp/cond';
import stubTrue from 'lodash/fp/stubTrue';
import constant from 'lodash/fp/constant';

const isAlmostOutOfToleranceRange = ({
  currentTemperature,
  minTemperature,
  maxTemperature
}) => currentTemperature === maxTemperature || currentTemperature === minTemperature;

const isOutOfToleranceRange = ({
   currentTemperature,
   minTemperature,
   maxTemperature
 }) => currentTemperature > maxTemperature || currentTemperature < minTemperature;

const manageContainerAppearance = cond([
  [isAlmostOutOfToleranceRange, constant('panel-warning')],
  [isOutOfToleranceRange, constant('panel-danger')],
  [stubTrue, constant('panel-success')]
]);

export default props =>
  <div className="col-xs-12 col-md-6 col-lg-4 beer-container" role="beer-container">
    <div className={`panel ${manageContainerAppearance(props)}`}>
      <div className="panel-heading">
        <h3 className="panel-title" role="container-spec">
          {`Container ${props.id} - temperature tolerance: ${props.minTemperature}°C - ${props.maxTemperature}°C`}
        </h3>
      </div>
      <div className="panel-body" role="current-temp">
        <h3>{`Current temperature: ${props.currentTemperature}°C`}</h3>
      </div>
    </div>
  </div>;
