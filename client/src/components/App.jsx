import React from 'react';
import ReactiveDom from '../ReactiveDom';
import {Observable, BehaviorSubject} from 'rxjs';
import BeerContainer from './BeerContainer';
import map from 'lodash/fp/map';

const {div: Div} = ReactiveDom;

const appState$ = new BehaviorSubject([]);

const callBeerContainerTemperaturesEndpoint = () => Observable.ajax({
  method: 'get',
  responseType: 'json',
  url: '/temp-status'
}).pluck('response').take(1);

const currentBeerContainersData$ = callBeerContainerTemperaturesEndpoint().merge(
  Observable.interval(5000).flatMap(callBeerContainerTemperaturesEndpoint)
);

currentBeerContainersData$.subscribe(beerContainerStatuses => appState$.next(beerContainerStatuses));

const App = () => {
  return (
    <Div className="row" role="app-container">
      {appState$.map(map((beerContainerStatus) => <BeerContainer {...beerContainerStatus} />))}
    </Div>
  );
};
export default App;
