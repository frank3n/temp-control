require('babel-core/register')();

const {JSDOM} = require('jsdom');

const dom = new JSDOM('<!DOCTYPE html><html><head></head><body></body></html>');

global.window = dom.window;
global.document = dom.window.document;
global.navigator = dom.window.navigator;
global.window.Object = Object;
global.window.Math = Math;
