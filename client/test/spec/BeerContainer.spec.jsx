import React from 'react';
import BeerContainer from 'components/BeerContainer';
import {mount} from 'enzyme';
import {expect} from 'chai';

describe('components/BeerContainer', () => {
  const renderBeerContainerInTemperature = currentTemperature =>
    mount(<BeerContainer id="1" minTemperature={3} maxTemperature={9} currentTemperature={currentTemperature} />);

  describe('when displayed', () => {
    it('should contain container spec', () => {
      const renderedBeerContainer = renderBeerContainerInTemperature(5);

      expect(renderedBeerContainer.find('[role="container-spec"]'))
        .to.have.text('Container 1 - temperature tolerance: 3°C - 9°C');
    });

    it('should display current temperature', () => {
      const renderedBeerContainer = renderBeerContainerInTemperature(5);

      expect(renderedBeerContainer.find('[role="current-temp"]'))
        .to.have.text('Current temperature: 5°C');
    });

    describe('when has temperature within tolerance range', () => {
      it('should indicate that everything is ok', () => {
        const renderedBeerContainer = renderBeerContainerInTemperature(5);

        expect(renderedBeerContainer.find('.panel')).to.have.className('panel-success');
      });
    });

    describe('when has temperature on the border of max tolerance range', () => {
      it('should indicate that temperature is close to being outside of tolerance range', () => {
        const renderedBeerContainer = renderBeerContainerInTemperature(9);

        expect(renderedBeerContainer.find('.panel')).to.have.className('panel-warning');
      });
    });

    describe('when has temperature on the border of min tolerance range', () => {
      it('should indicate that temperature is close to being outside of tolerance range', () => {
        const renderedBeerContainer = renderBeerContainerInTemperature(3);

        expect(renderedBeerContainer.find('.panel')).to.have.className('panel-warning');
      });
    });

    describe('when has temperature below min tolerance range', () => {
      it('should indicate that temperature is outside of tolerance range', () => {
        const renderedBeerContainer = renderBeerContainerInTemperature(2);

        expect(renderedBeerContainer.find('.panel')).to.have.className('panel-danger');
      });
    });

    describe('when has temperature above min tolerance range', () => {
      it('should indicate that temperature is outside of tolerance range', () => {
        const renderedBeerContainer = renderBeerContainerInTemperature(10);

        expect(renderedBeerContainer.find('.panel')).to.have.className('panel-danger');
      });
    });
  });
});
